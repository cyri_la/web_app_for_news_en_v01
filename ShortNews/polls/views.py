from django.shortcuts import render
from newsapi import NewsApiClient
import requests
# Create your views here.
# do not forget to change/ to delete the api key
api_key = ""

def index(request):
    global api_key
    newsapi = NewsApiClient(api_key=api_key)
    #top headlines for news based in uk
    top_headlines = newsapi.get_top_headlines(country = "gb")

    articles = top_headlines['articles']
    description = []
    news = []
    img = [] # but i dont think i will need this
    url = []
    for i in range(len(articles)):
        article = articles[i]
        news.append(article['title'])
        description.append(article["description"])
        img.append(article["urlToImage"])
        url.append(article['url'])


    data = zip(news, description, img, url)
    return render(request, "index.html", context={"data":data})

def news_pages(request, news_n):
    global api_key
    newsapi = NewsApiClient(api_key=api_key)
    top_headlines = newsapi.get_top_headlines(sources= news_n)

    articles = top_headlines['articles']
    description = []
    news = []
    img = [] # but i dont think i will need this
    url = []
    for i in range(len(articles)):
        article = articles[i]
        news.append(article['title'])
        description.append(article["description"])
        img.append(article["urlToImage"])
        url.append(article['url'])


    data = zip(news, description, img, url)
    return render(request,news_n+".html", context={"data":data})