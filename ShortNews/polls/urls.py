from django.urls import path
from .views import index, news_pages

urlpatterns = [
    path('', index, name = 'index'),
    path('axios/', lambda request: news_pages(request,"axios"), name = 'axios'),
    path('bbc-news/', lambda request: news_pages(request, "bbc-news"), name='bbc-news'),
    path('business-insider/', lambda request: news_pages(request, "business-insider"), name='business-insider'),
]
